package com.haochen.provider.auth.utils;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * @author YangChao
 */
public class PasswordHelper {


    public static String encryptPassword(String crdentials, String salt) {
        return new SimpleHash(Sha256Hash.ALGORITHM_NAME, crdentials, salt, 1).toString();
    }


    public static void main(String[] args) {
        String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        System.out.println(salt);
        System.out.println(PasswordHelper.encryptPassword("123456", salt));
    }
}
