package com.haochen.provider.auth.dto;

import java.io.Serializable;

/**
 * @author YangChao
 */
public class ResourceDto implements Serializable {

    private String parentResoCode;

    private String permission;

    private String path;

    private String componentPath;

    private String resoTypeCode;

    private String resoName;

    private String crtUserCode;

    public String getParentResoCode() {
        return parentResoCode;
    }

    public void setParentResoCode(String parentResoCode) {
        this.parentResoCode = parentResoCode;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getComponentPath() {
        return componentPath;
    }

    public void setComponentPath(String componentPath) {
        this.componentPath = componentPath;
    }

    public String getResoTypeCode() {
        return resoTypeCode;
    }

    public void setResoTypeCode(String resoTypeCode) {
        this.resoTypeCode = resoTypeCode;
    }

    public String getResoName() {
        return resoName;
    }

    public void setResoName(String resoName) {
        this.resoName = resoName;
    }

    public String getCrtUserCode() {
        return crtUserCode;
    }

    public void setCrtUserCode(String crtUserCode) {
        this.crtUserCode = crtUserCode;
    }
}
