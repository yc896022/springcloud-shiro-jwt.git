package com.haochen.provider.auth.entity;

import com.haochen.common.entity.BaseEntity;

import java.util.List;


/**
 * db_table: mst_roll_base
 * @author YangChao
 */
public class MstRollBaseEntity extends BaseEntity {
    private static final long serialVersionUID = -3470424544106972195L;

    //columns START
    /**
     * 业务基础角色基本id		db_column: mst_roll_base_id
     */
    private Integer mstRollBaseId;
    /**
     * 角色code		db_column: roll_code
     */
    private String rollCode;
    /**
     * 角色名称		db_column: roll_name
     */
    private String rollName;
    /**
     * 角色描述		db_column: roll_desc
     */
    private String rollDesc;
    /**
     * 角色权限		db_column: roll_permis
     */
    private String rollPermis;
    //columns END

    private List<String> resoCodeList;




    //get set START

    /**
     * 业务基础角色基本id		db_column: mst_roll_base_id
     */
    public void setMstRollBaseId(Integer mstRollBaseId) {
        this.mstRollBaseId = mstRollBaseId;
    }

    /**
     * 业务基础角色基本id		db_column: mst_roll_base_id
     */
    public Integer getMstRollBaseId() {
        return this.mstRollBaseId;
    }

    /**
     * 角色code		db_column: roll_code
     */
    public void setRollCode(String rollCode) {
        this.rollCode = rollCode;
    }

    /**
     * 角色code		db_column: roll_code
     */
    public String getRollCode() {
        return this.rollCode;
    }

    /**
     * 角色名称		db_column: roll_name
     */
    public void setRollName(String rollName) {
        this.rollName = rollName;
    }

    /**
     * 角色名称		db_column: roll_name
     */
    public String getRollName() {
        return this.rollName;
    }

    /**
     * 角色描述		db_column: roll_desc
     */
    public void setRollDesc(String rollDesc) {
        this.rollDesc = rollDesc;
    }

    /**
     * 角色描述		db_column: roll_desc
     */
    public String getRollDesc() {
        return this.rollDesc;
    }

    /**
     * 角色权限		db_column: roll_permis
     */
    public void setRollPermis(String rollPermis) {
        this.rollPermis = rollPermis;
    }

    /**
     * 角色权限		db_column: roll_permis
     */
    public String getRollPermis() {
        return this.rollPermis;
    }
    //get set End

    @Override
    public String toString() {
        return "MstRollBaseEntity [" +
                "MstRollBaseId=" + getMstRollBaseId() + ", " +
                "RollCode=" + getRollCode() + ", " +
                "RollName=" + getRollName() + ", " +
                "RollDesc=" + getRollDesc() + ", " +
                "RollPermis=" + getRollPermis() + ", " +
                "CrtTime=" + getCrtTime() + ", " +
                "CrtUserCode=" + getCrtUserCode() + ", " +
                "UpdTime=" + getUpdTime() + ", " +
                "UpdUserCode=" + getUpdUserCode() + ", " +
                "DelFlg=" + getDelFlg() + ", " +
                "VerNum=" + getVerNum() +
                "]";
    }

    public List<String> getResoCodeList() {
        return resoCodeList;
    }

    public void setResoCodeList(List<String> resoCodeList) {
        this.resoCodeList = resoCodeList;
    }
}


