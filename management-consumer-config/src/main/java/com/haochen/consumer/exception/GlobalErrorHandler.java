package com.haochen.consumer.exception;

import com.haochen.common.Response;
import com.haochen.common.SystemStatus;
import com.haochen.common.utils.ResultHelper;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yc082
 */
@ControllerAdvice
@ResponseBody
public class GlobalErrorHandler {

    @ExceptionHandler(AuthorizationException.class)
    public Response handlerAuthorizationException() {
        return ResultHelper.infoResp(SystemStatus.HTTP_NO_PERMISSION());
    }
}
