package com.haochen.consumer.auth.controller;

import com.haochen.common.Response;
import com.haochen.common.utils.ResultHelper;
import com.haochen.consumer.auth.dto.ResourceDto;
import com.haochen.consumer.auth.service.CfgSysResoService;
import com.haochen.consumer.base.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author YangChao
 */
@RestController
@RequestMapping("/resource")
public class ResourceController extends BaseController {

    private final CfgSysResoService resourceService;

    @Autowired
    public ResourceController(CfgSysResoService resourceService) {
        this.resourceService = resourceService;
    }

    @GetMapping("/findMenus")
    public Response findMenus() {
        return ResultHelper.successResp(resourceService.findAll());
    }


    @PostMapping("/createResource")
    public Response createResource(@RequestBody ResourceDto resourceDto) {
        resourceDto.setCrtUserCode(getCurrentUserCode());
        resourceService.createResource(resourceDto);
        return ResultHelper.successResp();
    }


}
