package com.haochen.consumer.auth.dto;

import java.io.Serializable;

/**
 * @author YangChao
 */
public class RoleDto implements Serializable {

    private String rollCode;

    private String rollName;

    private String rollDesc;

    private String[] resoCodes;

    private String crtUserCode;

    public String getRollName() {
        return rollName;
    }

    public void setRollName(String rollName) {
        this.rollName = rollName;
    }

    public String getRollDesc() {
        return rollDesc;
    }

    public void setRollDesc(String rollDesc) {
        this.rollDesc = rollDesc;
    }

    public String getCrtUserCode() {
        return crtUserCode;
    }

    public void setCrtUserCode(String crtUserCode) {
        this.crtUserCode = crtUserCode;
    }

    public String[] getResoCodes() {
        return resoCodes;
    }

    public void setResoCodes(String[] resoCodes) {
        this.resoCodes = resoCodes;
    }

    public String getRollCode() {
        return rollCode;
    }

    public void setRollCode(String rollCode) {
        this.rollCode = rollCode;
    }
}
