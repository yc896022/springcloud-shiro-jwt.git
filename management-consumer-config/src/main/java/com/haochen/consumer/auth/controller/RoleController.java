package com.haochen.consumer.auth.controller;

import com.haochen.common.Response;
import com.haochen.common.utils.ResultHelper;
import com.haochen.consumer.auth.dto.RoleDto;
import com.haochen.consumer.auth.dto.RoleListDto;
import com.haochen.consumer.auth.entity.MstRollBaseEntity;
import com.haochen.consumer.auth.service.CfgSysResoService;
import com.haochen.consumer.auth.service.MstRollBaseService;
import com.haochen.consumer.base.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YangChao
 */
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

    private final MstRollBaseService roleService;

    private final CfgSysResoService resourceService;

    @Autowired
    public RoleController(MstRollBaseService roleService, CfgSysResoService resourceService) {
        this.roleService = roleService;
        this.resourceService = resourceService;
    }


    @GetMapping("/roleList")
    public Response roleList(RoleListDto roleListDto) {
        Map<String, Object> paramMap = new HashMap<>(6);
        paramMap.put("start", roleListDto.getStart());
        paramMap.put("size", roleListDto.getPageSize());
        List<MstRollBaseEntity> roleList = roleService.findPage(paramMap);
        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("roleList", roleList);
        resultMap.put("roleCount", roleService.count(paramMap));
        return ResultHelper.successResp(resultMap);
    }


    @GetMapping("/findMenus")
    public Response findMenus(String roleCode) {
        Map<String, Object> paramMap = new HashMap<>(2);
        paramMap.put("menus", resourceService.findAll());
        paramMap.put("checkedNodes", resourceService.getCheckedNodesByRoleCode(roleCode));
        return ResultHelper.successResp(paramMap);
    }


    @PostMapping(value = "/updateRole")
    public Response updateRole(@RequestBody RoleDto roleDto) {
        roleDto.setCrtUserCode(getCurrentUserCode());
        roleService.updateRole(roleDto);
        return ResultHelper.successResp();
    }

}
