package com.haochen.consumer.auth.controller;

import com.haochen.common.Response;
import com.haochen.common.exception.SystemException;
import com.haochen.common.utils.ResultHelper;
import com.haochen.consumer.auth.dto.*;
import com.haochen.consumer.auth.entity.MstInterUserBaseEntity;
import com.haochen.consumer.auth.entity.TranInterUserTokenEntity;
import com.haochen.consumer.auth.service.MstInterUserBaseService;
import com.haochen.consumer.base.controller.BaseController;
import com.haochen.consumer.util.JwtUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YangChao
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    private final MstInterUserBaseService userService;

    @Autowired
    public UserController(MstInterUserBaseService userService) {
        this.userService = userService;
    }


    /**
     * 登陆接口
     * 校验权限的部分由shiro框架在dbrealm中完成，在这里我们只需要通过shiro验证并返回jwt token就好了
     */
    @PostMapping("/login")
    public Response login(@RequestBody LoginInputBean loginDto) {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(loginDto.getUsername(), loginDto.getPassword());
        subject.login(token);
        MstInterUserBaseEntity user = (MstInterUserBaseEntity) subject.getPrincipal();
        String newToken = JwtUtils.sign(user.getUserName(), user.getSalt(), 3600);
        TranInterUserTokenEntity tranInterUserTokenEntity = new TranInterUserTokenEntity();
        tranInterUserTokenEntity.setExpireTime(System.currentTimeMillis() + 3600 * 1000);
        tranInterUserTokenEntity.setJwtToken(newToken);
        tranInterUserTokenEntity.setUserCode(user.getInterUserCode());
        tranInterUserTokenEntity.setCrtUserCode(user.getInterUserCode());
        tranInterUserTokenEntity.setCrtTime(new Date());
        userService.updateJwtToken(tranInterUserTokenEntity);
        LoginOutputBean loginOutputBean = new LoginOutputBean();
        loginOutputBean.setToken(newToken);
        return ResultHelper.successResp(loginOutputBean);
    }

    @GetMapping("/info")
    public Response info() {
        return ResultHelper.successResp(userService.getUserInfo(getCurrentUser().getUserName()));
    }


    @RequiresPermissions("user:view")
    @GetMapping("/getInterUserListData")
    public Response getInterUserListData(UserListDto userListDto) {
        Map<String, Object> paramMap = new HashMap<>(6);
        paramMap.put("start", userListDto.getStart());
        paramMap.put("size", userListDto.getPageSize());
        List<InterUserDto> userList = userService.findPage(paramMap);
        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("userList", userList);
        resultMap.put("userCount", userService.count(paramMap));
        return ResultHelper.successResp(resultMap);
    }


    @RequiresPermissions("user:create")
    @PostMapping(value = "/createUser")
    public Response create(@RequestBody UpdateUserDto updateUserDto) {
        updateUserDto.setCrtUserCode(getCurrentUserCode());
        userService.createUser(updateUserDto);
        return ResultHelper.successResp();
    }


    @RequiresPermissions("user:update")
    @PostMapping(value = "/{interUserCode}/update")
    public Map<String, Object> update(@PathVariable("interUserCode") String interUserCode, MstInterUserBaseEntity user) {
        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("status", 200);
        try {
            MstInterUserBaseEntity m = userService.findByCode(interUserCode);
            user.setUpdUserCode(getCurrentUserCode());
            user.setVerNum(m.getVerNum());
            userService.updateUser(user);
        } catch (SystemException e) {
            resultMap.put("status", 500);
            resultMap.put("errorMsg", e.getMessage());
        }
        return resultMap;
    }


    @RequestMapping(value = "/changeMyPassword", method = RequestMethod.GET)
    public String showChangeMyPasswordForm(HttpServletRequest request, Model model) {
        model.addAttribute("user", userService.findByCode(getCurrentUserCode()));
        model.addAttribute("originUrl", request.getHeader("referer"));
        model.addAttribute("op", "修改密码");
        return "auth/user/changePassword";
    }


    /**
     * 退出登录
     */
    @GetMapping(value = "/logout")
    public ResponseEntity<Void> logout() {
        Subject subject = SecurityUtils.getSubject();
//        if(subject.getPrincipals() != null) {
//            UserDto user = (UserDto)subject.getPrincipals().getPrimaryPrincipal();
//            userService.deleteLoginInfo(user.getUsername());
//        }
        SecurityUtils.getSubject().logout();

        return ResponseEntity.ok().build();
    }


    /**
     * 接口请求会话是否alive
     */
    @RequestMapping("/checkSessionAlive")
    public String checkSessionAlive() {
        return getCurrentUserCode();
    }


    @GetMapping(value = "/getUerInfoByUserCode")
    public Response getUerInfoByUserCode(String userCode) {
        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("userBase", userService.findByCode(userCode));
        resultMap.put("userDtl", userService.findDtlByUserCode(userCode));
        return ResultHelper.successResp(resultMap);
    }
}
