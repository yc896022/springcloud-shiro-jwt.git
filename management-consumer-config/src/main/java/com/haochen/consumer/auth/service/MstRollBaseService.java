package com.haochen.consumer.auth.service;


import com.haochen.consumer.auth.dto.RoleDto;
import com.haochen.consumer.auth.entity.MstRollBaseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author YangChao
 */
@FeignClient(value = "management-provider", path = "role" )
public interface MstRollBaseService {

    @PostMapping(value = "/findByCode")
    MstRollBaseEntity findByCode(@RequestParam("rollCode") String rollCode);

    @PostMapping(value = "/findPage")
    List<MstRollBaseEntity> findPage(@RequestBody Map<String, Object> map);

    @PostMapping(value = "/count")
    Integer count(@RequestBody Map<String, Object> paramMap);

    @PostMapping(value = "/findBySubsyCodes")
    List<MstRollBaseEntity> findBySubsyCodes(@RequestParam("subsyCodes") String subsyCodes);

    @PostMapping(value = "/updateRole")
    void updateRole(@RequestBody RoleDto roleDto);

}
