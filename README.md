## springcloud-shiro-jwt

springboot2.0, springcloud, shiro, mybatis, redis, log4j2, vue, element-ui前端框架, jwt标准无状态token验证权限
项目主要目的是帮助想学习springcloud和shiro却不知道如何开始的朋友入门所搭建，在没有方向的时候， 把现成的框架下下来跑一跑，
读一读配置和源码必能让你获益良多，在掌握其中细节和配置原理后，项目经过二次开发可用于生产环境部署

姐妹版项目地址 [springboot-shiro-redis-session-cache](https://gitee.com/896022/shiroAuth.git)

### 项目演示地址

https://www.haochen.online/

### 安装步骤

* 1.运行需要先执行 init.sql，修改 management-provider-config 模块中 application.yml 的 jdbc 连接 
特别注意如果mysql是5.7以上版本，请去掉sql_mode中的ONLY_FULL_GROUP_BY 本项目用户支持多角色多部门，所以列表中有多表连接group by加载列表时会因上述问题报错
[mysqld] 
sql_mode = "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"

* 2.启动 Eureka 注册服务 management-eureka-server 类 EurekaApplication 中的 main 方法

* 3.启动内部接口服务 management-provider-config 类 ProviderApplication 中 main 方法

* 4.启动外部接口服务 management-consumer-config 类 ConsumerApplication 中 main 方法。

* 5.启用前端服务 需要 node 环境,进入 management-ui 目录,npm install 完了再 npm run dev 即可进行访问

* 6.登陆帐号 admin 密码 123456

### 部署说明

* 1.在项目根目录下运行mvn clean package -Ptest 其中test文件为服务器配置项在resources/test目录下 有需要自行修改

* 2.分批将每个模块下target目录下package 文件结尾的模块移动到服务器指定项目目录 在bin目录执行./starth.sh 依次启动

* 3.management-ui需要通过httpserver nginx或apache来部署 可自行查阅相关资料

### 讨论学习QQ群

*833825797*


