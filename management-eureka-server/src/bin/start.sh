#!/bin/sh
echo "-------------------------------------------"
echo "start server"
echo "-------------------------------------------"
# 设置项目代码路径
export CODE_HOME=$(cd ../`dirname $0`; pwd)
# 设置依赖路径
export CLASSPATH=${CODE_HOME}"/classes:"${CODE_HOME}"/lib/*"

# 启动类
export MAIN_CLASS=com.haochen.eureka.EurekaApplication
nohup java -classpath ${CLASSPATH} ${MAIN_CLASS} >/dev/null 2>&1 &
